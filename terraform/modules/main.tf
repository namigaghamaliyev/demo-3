#VPC Create
resource "aws_vpc" "demo-3-vpc" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = var.tenancy
  enable_dns_support = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = {
    Name = var.vpc_name
  }
}
#Public subnet for Applicaton
resource "aws_subnet" "demo3-app-subnet" {
  vpc_id     = aws_vpc.demo-3-vpc.id
  cidr_block = var.public_cidr
  availability_zone = var.public_az
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.vpc_name}-public-subnet"
  }
  depends_on = [
    aws_vpc.demo-3-vpc
  ]
}
#Private subnet-1 for DB
resource "aws_subnet" "demo3-db-subnet1" {
  vpc_id     = aws_vpc.demo-3-vpc.id
  cidr_block = var.private1_cidr
  availability_zone = var.private1_az

  tags = {
    Name = "${var.vpc_name}-db-subnet1"
  }
  depends_on = [
    aws_vpc.demo-3-vpc
  ]
}
#Private subnet-2 for DB
resource "aws_subnet" "demo3-db-subnet2" {
  vpc_id     = aws_vpc.demo-3-vpc.id
  cidr_block = var.private2_cidr
  availability_zone = var.private2_az

  tags = {
    Name = "${var.vpc_name}-db-subnet2"
  }
}
#Internet gateway create for Application-ec2
resource "aws_internet_gateway" "demo3-igw" {
  vpc_id = aws_vpc.demo-3-vpc.id

  tags = {
    Name =  "${var.vpc_name}-IGW"
  }
  depends_on = [
    aws_vpc.demo-3-vpc
  ]
}
#Route table for application-ec2
resource "aws_route_table" "demo3-app-route-table" {
  vpc_id = aws_vpc.demo-3-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo3-igw.id
  }

  tags = {
    Name =  "${var.vpc_name}-private-rt"
  }
  depends_on = [
    aws_vpc.demo-3-vpc,aws_subnet.demo3-app-subnet
  ]
}
resource "aws_route_table_association" "demo3-route-for-app" {
  subnet_id      = aws_subnet.demo3-app-subnet.id
  route_table_id = aws_route_table.demo3-app-route-table.id
  depends_on = [
    aws_route_table.demo3-app-route-table
  ]
}