output "vpc_id" {
    value = aws_vpc.demo-3-vpc.id
}

output "vpc_arn" {
    value = aws_vpc.demo-3-vpc.id
}

output "vpc_cidr" {
    value = aws_vpc.demo-3-vpc.cidr_block
}

output "igw_id" {
    value = aws_internet_gateway.demo3-igw.id
}

output "subnet_public_id" {
    value = aws_subnet.demo3-app-subnet.id
}

output "subnet_private1_id" {
    value = aws_subnet.demo3-db-subnet1.id
}

output "subnet_private2_id" {
    value = aws_subnet.demo3-db-subnet2.id
}
