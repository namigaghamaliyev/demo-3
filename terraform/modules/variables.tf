variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "CIDR for VPC"
}

variable "tenancy" {
  type        = string
  default     = "default"
  description = ""
}

variable "enable_dns_support" {
    default = true
}

variable "enable_dns_hostnames" {
    default = true
}

variable "vpc_name" {
  type        = string
  default     = "demo3-namiq"
  description = "vpc-demo3-terraform"
}

variable "public_cidr" {
  default = "10.0.1.0/24"
}
variable "private1_cidr" {
  default = "10.0.2.0/24"
}
variable "private2_cidr" {
  default = "10.0.3.0/24"
}

variable "private1_az" {}
variable "private2_az" {}
variable "public_az" {}
