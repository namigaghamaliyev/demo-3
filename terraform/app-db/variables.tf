#EC2 variables
variable "aws_region" {
    default = ""
}

variable "ec2_count" {
  default = "1"
}

variable "ami_id" {
  description = "AMI which made with packer,includes mysql client,java"
    default = ""
}

variable "instance_type" {
  default = ""
}
variable "associate_public_ip_address" {
  default = true
}

 variable "aws_access_key" {
   default = ""
 }

 variable "aws_secret_key" {
   default = ""
 }

#DB variables

variable "db_instance_class" {
  default = ""
}

variable "db_identifier" {
  default = ""
}

variable "mysql_name" {
  default = ""
}

variable "mysql_user" {
  default = ""
}

variable "mysql_pass" {
  default = ""
}
 variable "mysql_port" {
   default = ""
 }


