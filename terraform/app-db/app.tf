#EC2 for petclinic app
resource "aws_instance" "ec2-app-petclinic" {
    ami           = var.ami_id
    instance_type = var.instance_type
    subnet_id     = module.vpc.subnet_public_id
    vpc_security_group_ids = [ aws_security_group.app-sg.id ]
    
    tags = {
        Name = "ec2-app for applicatin"
    }

    depends_on = [ module.vpc.vpc_id, module.vpc.igw_id, aws_db_instance.demo3-database-db ]

    user_data = <<EOF
#!/bin/sh
cd /home/ec2-user
sudo yum update
export MYSQL_USER=${var.mysql_user} 
export MYSQL_PASS=${var.mysql_pass}
export MYSQL_URL=jdbc:mysql://${aws_db_instance.demo3-database-db.address}:${var.mysql_port}/${var.mysql_name}
export AWS_ACCESS_KEY_ID=${var.aws_access_key}
export AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}

aws s3api get-object --bucket demo3-namiq --key petclinic.jar petclinic.jar
#aws s3 cp s3://demo3-namiq/petclinic.jar .
java -Dspring.profiles.active=mysql -jar petclinic.jar 

EOF
}
#Security group for app_ec2
resource "aws_security_group" "app-sg" {
  name        = "security-group"
  description = "allow inbound access to the EC2 instance"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "petclinic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    description = "HTTPS"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
