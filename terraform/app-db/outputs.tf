output "ec2-public-dns" {
    description = "The public dns url of ec2"
    value = aws_instance.ec2-app-petclinic.public_dns
}

output "ec2-public-ip" {
    description = "The public ip assigned to ec2 instance which runs application"
    value = aws_instance.ec2-app-petclinic.public_ip
}

output "db-address" {
    value = aws_db_instance.demo3-database-db.address
}
output "db-domain" {
    value = aws_db_instance.demo3-database-db.domain
}

output "db-endpoint" {
    value = aws_db_instance.demo3-database-db.endpoint
}

output "db-name" {
    value = aws_db_instance.demo3-database-db.name
}

output "db-port" {
    value = aws_db_instance.demo3-database-db.port
}