#Parameter group for RDS-DB
resource "aws_db_parameter_group" "demo3-db-pg-group" {
  name   = "db-pg"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}
#Subnet-group for db
resource "aws_db_subnet_group" "demo3-db-subnet-group" {
  name       = "demo3-namiq-db-subnet"
  subnet_ids = [ module.vpc.subnet_private1_id , module.vpc.subnet_private2_id ]
  tags = {
    Name = "My DB subnet group"
  }
}
#Security group for db
resource "aws_security_group" "db-sg" {
  name        = "db-security-group"
  description = "allow inbound access to the database"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = [ module.vpc.vpc_cidr ]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = [ module.vpc.vpc_cidr ]
  }
}
#RDS for app
resource "aws_db_instance" "demo3-database-db" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = var.db_instance_class
  identifier           = var.db_identifier
  name                 = var.mysql_name
  username             = var.mysql_user
  password             = var.mysql_pass
  parameter_group_name = aws_db_parameter_group.demo3-db-pg-group.id
  db_subnet_group_name = aws_db_subnet_group.demo3-db-subnet-group.id
  vpc_security_group_ids = [ aws_security_group.db-sg.id ]
  publicly_accessible  = false
  skip_final_snapshot  = true
  multi_az             = false
  
}