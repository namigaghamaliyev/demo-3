variable "aws_access_key" {
  type        = string
  description = "IAM user access-key."
  default     = ""
  sensitive   = true
}
variable "aws_secret_key" {
  type        = string
  description = "IAM user secret-key"
  default     = ""
  sensitive   = true
}
variable "ssh_username" {
  type        = string
  description = "The username to use to authenticate over SSH."
  default     = ""
  sensitive   = true
}
variable "vpc_id" {
  type        = string
  description = "vpc-id"
  default     = ""
}
variable "subnet_id" {
  type        = string
  description = "subnet-id"
  default     = ""
}
variable "region" {
  type        = string
  description = "region"
  default     = ""
}
variable "ami_name" {
  type        = string
  description = "AMI name"
  default     = ""
}
variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = ""
}
variable "sg_id" {
  type        = string
  description = "Security-group-id"
  default     = ""
}

source "amazon-ebs" "ec2-app-demo" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  ssh_username = var.ssh_username
  vpc_id = var.vpc_id
  subnet_id = var.subnet_id
  ami_name = var.ami_name
  instance_type = var.instance_type
  region = var.region
  communicator = "ssh"
  ssh_timeout = "5m"
  associate_public_ip_address = true
  security_group_id = var.sg_id
  force_deregister = true
  force_delete_snapshot = true
  source_ami_filter {
    filters = {
      architecture        = "x86_64"
      name                = "*amzn2-ami-hvm-2.0.*-ebs"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["137112412989"]
  }
  launch_block_device_mappings {
    device_name = "/dev/sda1"
    volume_size = 8
    volume_type = "gp2"
    delete_on_termination = true
  }

}

build {
  sources = ["source.amazon-ebs.ec2-app-demo"]

  provisioner "shell" {
    inline = [
        "sudo yum update -y",
        "sudo yum install -y https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm",
        "sudo yum install -y mysql-community-client git",
        "sudo amazon-linux-extras install java-openjdk11",
        
      ]
  }
}