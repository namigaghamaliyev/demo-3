import requests
import os
import time

url = f'http://{os.environ["EC2_PUBLIC_IP"]}:8080/actuator/health'

for i in range(5):
    response = str(requests.get(url))
    if "200" in response:
        print("Running")
        break
    else:
        print(f"Cant run url: {url}")
        time.sleep(10)
else:
    print("Not work")
    exit(1)
